console.log("Hello from JS!")

let params = new URLSearchParams(window.location.search)

let id = params.get('courseId')
console.log(id)

let token = localStorage.getItem('token')
console.log(token)

const name = document.querySelector("#courseName")
const desc = document.querySelector("#courseDesc")
const price = document.querySelector("#coursePrice")
const enroll = document.querySelector("#enrollmentContainer")

fetch(`https://still-shelf-35067.herokuapp.com/api/courses/${id}`).then(res=>res.json()).then(data=>{
	console.log(data)

	name.innerHTML = data.name
	desc.innerHTML = data.description
	price.innerHTML = data.price
	enroll.innerHTML = `<a id="enrollButton" class="btn btn-dark text-white btn-block">Enroll</a>`
})

document.querySelector("#enrollButton").addEventListener('click', () => {
	fetch(`https://still-shelf-35067.herokuapp.com/api/users/enroll`, {
		method: 'POST',
			headers: { 
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}` 
			},
			body: JSON.stringify({
				courseId: id
			})
		}).then(res => {
			return res.json()
		}).then(data => {
			if (data === true){
				alert("Successfully enrolled in this course!")
				window.location.replace("./courses.html")
			} else {
				alert("Something went wrong!")
			}
		})
	})

/*
enroll.addEventListener('click', (e) => {
	e.preventDefault()
	
	fetch(`http://localhost:4000/api/users/enroll`, {
		method: 'POST',
		headers: { 
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}` 
		},
		body: JSON.stringify({
			userId: token,
			courseId: id 
		})
	}).then(res => {
		return res.json()
	}).then(data => {
		console.log(data)
		if (data === true) {
			alert('Success!')
		} else {
			alert('Error')
		}
	})
});
*/