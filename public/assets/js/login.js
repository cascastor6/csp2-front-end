console.log ("Hello from JS")

let loginForm = document.querySelector("#loginUser")

loginForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let email = document.querySelector("#userEmail").value
	let password = document.querySelector("#password").value

	console.log(email)
	console.log(password)

	if(email =="" || password == ""){
		Swal.fire({
			icon: "error", 
			title: "Email/Password Missing!",
			text: "Please input your email and/or password.",
			timer: 10000,
			timerProgressBar: true,
							  didOpen: () => {
							    Swal.showLoading()
							    timerInterval = setInterval(() => {
							      const content = Swal.getContent()
							      if (content) {
							        const b = content.querySelector('b')
							        if (b) {
							          b.textContent = Swal.getTimerLeft()
							        }
							      }
							    }, 100)
							  },
							  willClose: () => {
							    clearInterval(timerInterval)
							  }
			
		})
	} else {
		fetch('https://still-shelf-35067.herokuapp.com/api/users/login', {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				"emailAddress": email,
				"password": password
			})
		}).then(res => {
			return res.json()
		}).then(data => {
			console.log(data.access)
		if(data.access){
			localStorage.setItem('token',data.access)
			
			Swal.fire({
			icon: "success", 
			title: "Successfully logged in!",
			timer: 10000,
			timerProgressBar: true,
							  didOpen: () => {
							    Swal.showLoading()
							    timerInterval = setInterval(() => {
							      const content = Swal.getContent()
							      if (content) {
							        const b = content.querySelector('b')
							        if (b) {
							          b.textContent = Swal.getTimerLeft()
							        }
							      }
							    }, 100)
							  },
							  willClose: () => {
							    clearInterval(timerInterval)
							  }
			
		})

			fetch(`https://still-shelf-35067.herokuapp.com/api/users/details`, {
				headers: {
					'Authorization':`Bearer ${data.access}`
				}
			}).then(res=> {
				return res.json()
			}).then(data=>{
				console.log(data)
				localStorage.setItem("id", data._id)
				localStorage.setItem("isAdmin",data.isAdmin)
				console.log("Items are set inside the local storage.")

				window.location.replace('./courses.html')
			})
		} else {
			alert("Something went wrong, check your credentials.")
		}
		})

	}
})