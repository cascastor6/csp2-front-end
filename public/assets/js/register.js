console.log("Testing 1..2..3..")

const registerForm = document.querySelector("#registerUser")

registerForm.addEventListener("submit", (e) => {

	e.preventDefault();

	const firstName = document.querySelector("#firstName").value
	const lastName = document.querySelector("#lastName").value
	const userEmail = document.querySelector("#userEmail").value
	const mobileNumber = document.querySelector("#mobileNumber").value
	const password1 = document.querySelector("#password1").value
	const password2 = document.querySelector("#password2").value

	

	if((password1 !== "" && password2 !== "") && (password1===password2) && (mobileNumber.length===11)){
		// console.log("Passwords match.");

		fetch("https://still-shelf-35067.herokuapp.com/api/users/email-exists", {
			method:"POST",
			header: {
				"Content-Type":"application/json"
			},
			body:JSON.stringify({
				"emailAddress": userEmail
			})
		}).then(res=>res.json()).then(data=>{
			console.log(data)
			if(data === false){
				fetch("https://still-shelf-35067.herokuapp.com/api/users/register", {
					method: "POST",
					headers: {
						"Content-Type":"application/json"
					},
					body: JSON.stringify({
						"firstName": firstName,
						"lastName": lastName,
						"mobileNumber": mobileNumber,
						"password": password1
					})
				}).then(res => {
		        	return res.json()
		        }).then(data => {
		        	console.log(data)
		        	if(data === true){
		        		Swal.fire({
							icon: "success", 
							title: "Account Registered!",
							text: "New account has been successfully registered!",
							timer: 3000,
							  timerProgressBar: true,
							  didOpen: () => {
							    Swal.showLoading()
							    timerInterval = setInterval(() => {
							      const content = Swal.getContent()
							      if (content) {
							        const b = content.querySelector('b')
							        if (b) {
							          b.textContent = Swal.getTimerLeft()
							        }
							      }
							    }, 100)
							  },
							  willClose: () => {
							    clearInterval(timerInterval)
							  }
						})
					}else {	
						 Swal.fire({
							icon: "error", 
							title: "Error!",
							text: "Something went wrong in the registration."
						})
		        	}
		        })

			} else {
				Swal.fire({
					icon: "warning", 
					title: "Email Already Exists!",
					text: "This email has already been registered under another account."
				})
			}
		})

	} else {
		Swal.fire({
			icon: "error", 
			title: "Error!",
			text: "Error in input. Passwords must match and mobile number must be valid.",
			timerProgressBar: true,
							  didOpen: () => {
							    Swal.showLoading()
							    timerInterval = setInterval(() => {
							      const content = Swal.getContent()
							      if (content) {
							        const b = content.querySelector('b')
							        if (b) {
							          b.textContent = Swal.getTimerLeft()
							        }
							      }
							    }, 100)
							  },
							  willClose: () => {
							    clearInterval(timerInterval)
							  }
			
		})
	}
})
	