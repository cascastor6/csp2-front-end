console.log("Hello from JS!")

let token = localStorage.getItem('token')
console.log(token)

let profile = document.querySelector('#profileContainer')

if (!token || token === null){
	Swal.fire({
			icon: "error", 
			title: "Login Required!",
			text: "You must login before you can view your profile."
		})
	window.location.href="./login.html"
} else {
	//console.log("Token retrieved!")
	fetch(`https://still-shelf-35067.herokuapp.com/api/users/details`, {
		method: "GET",
		headers: {
			"Content-Type": "application/json",
			"Authorization": `Bearer ${token}`
		}
	}).then(res=>res.json()).then(data => {
		console.log(data)
		profile.innerHTML = `
			<div class="col-md-12">
				<section class="jumbotron my-5">
					<h3 class="text-center text-dark">First Name: ${data.firstName}</h3>
					<h3 class="text-center text-dark">Last Name: ${data.lastName}</h3>
					<h3 class="text-center text-dark">Email: ${data.emailAddress}</h3>
					<table class="table">
						<thead>
							<tr>
								<th>Course ID:</th>
								<th>Enrolled On:</th>
								<th>Status:</th>
								<tbody></tbody>
							</tr>
						</thead>
					</table>
				</section>
			</div>		
		`
	})
}