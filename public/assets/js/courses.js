console.log("Hello from JS!")

let container = document.querySelector("#coursesContainer")
let modalButton = document.querySelector('#adminButton')
let isAdmin = localStorage.getItem("isAdmin")
let cardFooter;

if(isAdmin == "false" || !isAdmin){
   //if a user is a regular user, do not show the addcourse button.
   modalButton.innerHTML = null
   
}else{
    modalButton.innerHTML = `
      <div class="col-md-2 offset-md-10"><a href="./addCourse.html" class="btn btn-block btn-danger">Add Course</a>
      </div>
    `
}

fetch('https://still-shelf-35067.herokuapp.com/api/courses/').then(res => res.json()).then(data => {
	console.log(data)

	let courseData;

	if(data.length <1){
		courseData="No Course Available"
	} else {
		courseData=data.map(course => {
			console.log(course._id); 

			if(isAdmin == "false" || !isAdmin){
				cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-danger text-white btn-block">View course details</a>`
				
			} else {
				cardFooter=`<a href="./editCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block">Edit course details</a>
				<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block">Disable course</a>`
			}

			return (
				`
				<div class="col-md-6 my-3">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">${course.name}</h5>
							<p class="card-text text-left">${course.description}</p>
							<p class="card-text text-left">P${course.price}</p>
						</div>
						<div class="card-footer">
							${cardFooter}
						</div>
					</div>
				</div>
				`
			)
		}).join(``)
	}
	container.innerHTML= courseData;
})
