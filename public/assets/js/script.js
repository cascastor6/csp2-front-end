console.log(`Hello from script.js`)

let navItem = document.querySelector('#navSession')

let userToken = localStorage.getItem('token')
console.log(userToken)

if(!userToken) {
	navItem.innerHTML = 
	`
	<li class="nav-item" id="navSession">
		<a href="../index.html" class="nav-link">Home</a>
	</li>
	<li class="nav-item">
		<a href="./courses.html" class="nav-link">Courses</a>
	</li>	
	<li class="nav-item">
		<a href="./login.html" class="nav-link">Log In</a>
	</li>
	<li class="nav-item">
		<a href="./register.html" class="nav-link">Register</a>
	</li>
	`
} else {
	navItem.innerHTML =	
	`
	<li class="nav-item" id="navSession">
		<a href="../index.html" class="nav-link">Home</a>
	</li>
	<li class="nav-item">
		<a href="./courses.html" class="nav-link">Courses</a>
	</li>
	<li class="nav-item">
		<a href="./profile.html" class="nav-link">Profile</a>
	</li>
	<li class="nav-item">
		<a href="./logout.html" class="nav-link">Log Out</a>
	</li>

	`
}